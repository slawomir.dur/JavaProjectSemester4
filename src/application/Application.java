package application;

import java.awt.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import com.l2fprod.common.swing.JOutlookBar;
import java.text.SimpleDateFormat;
import org.freixas.jcalendar.JCalendarCombo;
import org.freixas.jcalendar.DateEvent;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableCellRenderer;

import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.*;

import java.awt.*;
import java.io.PrintWriter;
import java.util.List;

import org.jfree.chart.ChartPanel;

import com.l2fprod.common.swing.JTipOfTheDay;
import com.l2fprod.common.swing.tips.DefaultTip;
import com.l2fprod.common.swing.tips.DefaultTipModel;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * The class representing the GUI.
 */
public class Application extends JFrame {
    private JPanel statusBar;
    private JLabel statusLabel = new JLabel("Status Bar");
    private JMenuItem helpItem;
    private JMenuItem aboutTheAuthorItem;
    private JLabel inputLabel;
    private JTextField inputField;
    private JSlider rowSlider;
    private JSlider columnSlider;
    private JTable table;
    private JButton resetButton;
    private JButton addButton;
    private JButton toFileButton;
    private JPanel centrePanel;
    private JTextArea resultArea;
    private CustomTableModel tableModel = new CustomTableModel();
    private JPanel chartPanel = new JPanel();
    private Database database;
    private static final Logger logger = Logger.getLogger(Application.class.getName());

    Application(Database db) {
        database = db;
        DOMConfigurator.configure(Objects.requireNonNull(getClass().getClassLoader().getResource("log4j.xml")));

        tableModel.setChartPanel(chartPanel);
        setIcon();
        setLayout(new BorderLayout());
        setMenu();
        setToolBar();
        setStatusBar();
        setCentre();
        setWest();
        setFrame();
        addListenerToSliders();
        showTip();
    }

    private void setIcon() {
        ImageIcon icon = new ImageIcon(
                Objects.requireNonNull(getClass().getClassLoader().getResource("application_icon.png")));
        if (icon != null)
            this.setIconImage(icon.getImage());
        System.out.println(icon);
    }

    /**
     * Sets the window
     */
    private void setFrame() {
        setSize(new Dimension(780, 520));
        setResizable(false);
        centreWindow();
        setTitle("Java Project");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    /**
     * Places the window in the centre of the screen
     */
    private void centreWindow() {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (int) (dimension.getWidth() - this.getWidth()) / 2;
        int y = (int) (dimension.getHeight() - this.getHeight()) / 2;
        this.setLocation(x, y);
    }

    /**
     * Sets the status bar.
     */
    private void setStatusBar() {
        statusBar = new JPanel();
        statusBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
        add(statusBar, BorderLayout.SOUTH);
        statusBar.setPreferredSize(new Dimension(getHeight(), 16));
        statusBar.setLayout(new BoxLayout(statusBar, BoxLayout.X_AXIS));
        statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
        statusBar.add(statusLabel);
    }

    class SettingsDialog extends JDialog {
        SettingsDialog(Component parent) {
            setWidgets();
            setLocationRelativeTo(parent);
            setTitle("Settings");
            setSize(300, 200);
        }

        public void display() {
            setVisible(true);
        }

        private void setWidgets() {
            getContentPane().setLayout(new GridBagLayout());

            JPanel mainPanel = new JPanel();
            mainPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
            mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
            add(mainPanel);

            int width = 0;

            JPanel loginPanel = new JPanel();
            loginPanel.setLayout(new BoxLayout(loginPanel, BoxLayout.X_AXIS));
            JLabel loginLabel = new JLabel("Login:");
            loginLabel.setBorder(new EmptyBorder(0, 0, 0, 20));
            loginPanel.add(loginLabel);

            width = (width < loginLabel.getPreferredSize().width) ? loginLabel.getPreferredSize().width : width;

            JTextField loginField = new JTextField();
            loginPanel.add(loginField);

            JPanel passwordPanel = new JPanel();
            passwordPanel.setLayout(new BoxLayout(passwordPanel, BoxLayout.X_AXIS));
            JLabel passwordLabel = new JLabel("Password:");
            passwordLabel.setBorder(new EmptyBorder(0, 0, 0, 20));
            passwordPanel.add(passwordLabel);
            JPasswordField passwordField = new JPasswordField();
            passwordPanel.add(passwordField);

            width = (width < passwordLabel.getPreferredSize().width) ? passwordLabel.getPreferredSize().width : width;

            JPanel confirmPanel = new JPanel();
            confirmPanel.setLayout(new BoxLayout(confirmPanel, BoxLayout.X_AXIS));
            JLabel confirmLabel = new JLabel("Confirm password:");
            confirmLabel.setBorder(new EmptyBorder(0, 0, 0, 20));
            confirmPanel.add(confirmLabel);
            JPasswordField confirmPasswordField = new JPasswordField();
            confirmPanel.add(confirmPasswordField);

            width = (width < confirmLabel.getPreferredSize().width) ? confirmLabel.getPreferredSize().width : width;
            int height = confirmLabel.getPreferredSize().height;

            loginLabel.setMinimumSize(new Dimension(width, height));
            loginLabel.setPreferredSize(new Dimension(width, height));
            loginLabel.setMaximumSize(new Dimension(width, height));

            passwordLabel.setMinimumSize(new Dimension(width, height));
            passwordLabel.setPreferredSize(new Dimension(width, height));
            passwordLabel.setMaximumSize(new Dimension(width, height));

            confirmLabel.setMinimumSize(new Dimension(width, height));
            confirmLabel.setPreferredSize(new Dimension(width, height));
            confirmLabel.setMaximumSize(new Dimension(width, height));

            int fieldWidth = loginField.getPreferredSize().width + 100;
            int fieldHeight = loginField.getPreferredSize().height;

            loginField.setMinimumSize(new Dimension(fieldWidth, fieldHeight));
            loginField.setPreferredSize(new Dimension(fieldWidth, fieldHeight));
            loginField.setMaximumSize(new Dimension(fieldWidth, fieldHeight));

            passwordField.setMinimumSize(new Dimension(fieldWidth, fieldHeight));
            passwordField.setPreferredSize(new Dimension(fieldWidth, fieldHeight));
            passwordField.setMaximumSize(new Dimension(fieldWidth, fieldHeight));

            confirmPasswordField.setMinimumSize(new Dimension(fieldWidth, fieldHeight));
            confirmPasswordField.setPreferredSize(new Dimension(fieldWidth, fieldHeight));
            confirmPasswordField.setMaximumSize(new Dimension(width, fieldHeight));

            System.out.println(confirmPasswordField.getPreferredSize());

            JPanel savePanel = new JPanel();
            savePanel.setLayout(new BoxLayout(savePanel, BoxLayout.X_AXIS));
            JButton saveButton = new JButton("Save settings");
            saveButton.addActionListener(event -> {
                String pwd = new String(passwordField.getPassword());
                String confpwd = new String(confirmPasswordField.getPassword());
                if (pwd.equals(confpwd) && !pwd.isEmpty() && !confpwd.isEmpty()) {
                    String login = loginField.getText();
                    Credentials credentials = new Credentials(login, pwd);
                    if (database.setCredentials(credentials)) {
                        JOptionPane.showMessageDialog(this, "Credentials set correctly.");
                        loginField.setText("");
                        passwordField.setText("");
                        confirmPasswordField.setText("");
                    } else {
                        JOptionPane.showMessageDialog(this, "Credentials could not be set");
                    }
                } else {
                    JOptionPane.showMessageDialog(this, "Please, confirm password correctly.");
                }
            });
            savePanel.setBorder(new EmptyBorder(10, 30, 10, 30));
            savePanel.add(saveButton);

            mainPanel.add(loginPanel);
            mainPanel.add(Box.createVerticalStrut(10));
            mainPanel.add(passwordPanel);
            mainPanel.add(Box.createVerticalStrut(10));
            mainPanel.add(confirmPanel);
            mainPanel.add(Box.createVerticalStrut(10));
            mainPanel.add(savePanel);
        }

    }

    class TechnicalSupportContactDialog extends JDialog {
        TechnicalSupportContactDialog(Component parent) {
            setWidgets();
            setLocationRelativeTo(parent);
            setTitle("Settings");
            setSize(490, 515);
        }

        public void display() {
            setVisible(true);
        }

        private void setWidgets() {
            getContentPane().setLayout(new GridBagLayout());
            JPanel mainPanel = new JPanel();
            mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

            JPanel fromPanel = new JPanel();
            fromPanel.setLayout(new BoxLayout(fromPanel, BoxLayout.X_AXIS));
            JLabel fromLabel = new JLabel("From:");
            fromLabel.setBorder(new EmptyBorder(0, 0, 0, 20));
            JTextField fromField = new JTextField();

            fromPanel.add(fromLabel);
            fromPanel.add(fromField);

            JPanel subjectPanel = new JPanel();
            subjectPanel.setLayout(new BoxLayout(subjectPanel, BoxLayout.X_AXIS));
            JLabel subjectLabel = new JLabel("Subject:");
            subjectLabel.setBorder(new EmptyBorder(0, 0, 0, 20));
            JTextField subjectField = new JTextField();

            subjectPanel.add(subjectLabel);
            subjectPanel.add(subjectField);

            int width = (fromLabel.getPreferredSize().width > subjectLabel.getPreferredSize().width)
                    ? fromLabel.getPreferredSize().width
                    : subjectLabel.getPreferredSize().width;
            int height = fromLabel.getPreferredSize().height;

            fromLabel.setMinimumSize(new Dimension(width, height));
            fromLabel.setPreferredSize(new Dimension(width, height));
            fromLabel.setMaximumSize(new Dimension(width, height));

            subjectLabel.setMinimumSize(new Dimension(width, height));
            subjectLabel.setPreferredSize(new Dimension(width, height));
            subjectLabel.setMaximumSize(new Dimension(width, height));

            width = 350;
            height = fromField.getPreferredSize().height;

            fromField.setMinimumSize(new Dimension(width, height));
            fromField.setPreferredSize(new Dimension(width, height));
            fromField.setMaximumSize(new Dimension(width, height));

            subjectField.setMinimumSize(new Dimension(width, height));
            subjectField.setPreferredSize(new Dimension(width, height));
            subjectField.setMaximumSize(new Dimension(width, height));

            JPanel mailPanel = new JPanel();
            mailPanel.setLayout(new BoxLayout(mailPanel, BoxLayout.Y_AXIS));
            JPanel mailLabelPanel = new JPanel();
            mailLabelPanel.setLayout(new BoxLayout(mailLabelPanel, BoxLayout.X_AXIS));
            JLabel mailLabel = new JLabel("Text:");
            mailLabelPanel.add(mailLabel);
            mailLabelPanel.add(Box.createRigidArea(new Dimension(width + 15, height)));
            JTextArea mailArea = new JTextArea();
            JScrollPane textPane = new JScrollPane(mailArea);
            textPane.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));

            width = fromPanel.getPreferredSize().width;
            height = 350;

            textPane.setMinimumSize(new Dimension(width, height));
            textPane.setPreferredSize(new Dimension(width, height));
            textPane.setMaximumSize(new Dimension(width, height));

            mailPanel.add(mailLabelPanel);
            mailPanel.add(textPane);

            JPanel sendPanel = new JPanel();
            sendPanel.setLayout(new BoxLayout(sendPanel, BoxLayout.X_AXIS));
            sendPanel.add(Box.createRigidArea(new Dimension(250, 25)));
            JButton sendButton = new JButton("Send");
            sendButton.addActionListener(event -> {
                Credentials credentials = database.getCredentials();
                String from = fromField.getText();
                String subject = subjectField.getText();
                String text = mailArea.getText();
                Mail mail = new Mail(from, "", subject, text);
                if (MailSender.send(credentials, mail)) {
                    JOptionPane.showMessageDialog(this, "Message has been sent!");
                } else {
                    JOptionPane.showMessageDialog(this, "Message could not be sent due to internal error");
                }
            });
            sendPanel.add(sendButton);

            mainPanel.add(fromPanel);
            mainPanel.add(Box.createVerticalStrut(10));
            mainPanel.add(subjectPanel);
            mainPanel.add(Box.createVerticalStrut(10));
            mainPanel.add(mailPanel);
            mainPanel.add(Box.createVerticalStrut(10));
            mainPanel.add(sendPanel);

            add(mainPanel);
        }

    }

    /** Sets the menu */
    private void setMenu() {
        JMenu fileMenu = new JMenu("File");

        JMenuItem newItem = new JMenuItem("New");
        newItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
        newItem.addActionListener(event->{
            tableModel.resetData();
            table.repaint();
        });

        JMenuItem openItem = new JMenuItem("Open");
        openItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
        openItem.addActionListener(event->{
            try{
                 tableModel.loadFromFile(this);
                table.repaint();
            } catch (IOException exc) {
                JOptionPane.showMessageDialog(this, exc.getMessage());
            }
           
        });

        JMenuItem saveFileItem = new JMenuItem("Save as...");
        saveFileItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
        saveFileItem.addActionListener(event -> {
            try {
                tableModel.saveToFile();
            } catch (FileNotFoundException exc) {
                JOptionPane.showMessageDialog(this, exc.getMessage());
            }
        });
        fileMenu.add(newItem);
        fileMenu.add(openItem);
        fileMenu.add(saveFileItem);

        JMenu editMenu = new JMenu("Edit");

        JMenuItem addItem = new JMenuItem("Add to table");
        addItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
        addItem.addActionListener(event -> {
            addAction();
        });

        JMenuItem resetTable = new JMenuItem("Reset table");
        resetTable.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
        resetTable.addActionListener(event -> {
            tableModel.resetData();
            table.repaint();
        });
        
        JMenuItem randomItem = new JMenuItem("Generate random values");
        randomItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.ALT_MASK));
        randomItem.addActionListener(event -> {
            tableModel.generateRandomData();
            table.repaint();
        });

        editMenu.add(addItem);
        editMenu.add(randomItem);
        editMenu.add(resetTable);

        JMenu calculationsMenu = new JMenu("Calculations");

        JMenuItem sumItem = new JMenuItem("Show sum");
        sumItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.ALT_MASK));
        sumItem.addActionListener(event -> {
            resultArea.setText(Double.toString(tableModel.sumElements()));
            logInfo("Sum chosen");
        });
        JMenuItem avgItem = new JMenuItem("Show average");
        avgItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.ALT_MASK));
        avgItem.addActionListener(event -> {
            resultArea.setText(Double.toString(tableModel.avgElements()));
            logInfo("Average chosen");
        });
        JMenuItem extremesItem = new JMenuItem("Show extremes");
        extremesItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
        extremesItem.addActionListener(event -> {
            double min = tableModel.minElement();
            double max = tableModel.maxElement();
            resultArea.setText("Min: " + min + " Max: " + max);
            logInfo("Min and max chosen");
        });

        calculationsMenu.add(sumItem);
        calculationsMenu.add(avgItem);
        calculationsMenu.add(extremesItem);

        JMenu helpMenu = new JMenu("Help");
        helpItem = new JMenuItem("Help");
        helpItem.addActionListener(event -> {
            openHelp();
        });
        aboutTheAuthorItem = new JMenuItem("About the author");
        aboutTheAuthorItem.addActionListener((ActionEvent event) -> {
            JOptionPane.showMessageDialog(this, "Author: Slawomir Durawa\nIndex nr.: U-11909");
        });
        JMenuItem contactSupportItem = new JMenuItem("Contact technical support");
        contactSupportItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.SHIFT_MASK));
        contactSupportItem.addActionListener(event -> {
            TechnicalSupportContactDialog dialog = new TechnicalSupportContactDialog(this);
            dialog.display();
        });
        JMenuItem settingsItem = new JMenuItem("Settings");
        settingsItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, ActionEvent.SHIFT_MASK));
        settingsItem.addActionListener(event->{
            SettingsDialog dialog = new SettingsDialog(this);
            dialog.display();
        });

        helpMenu.add(helpItem);
        helpMenu.add(aboutTheAuthorItem);
        helpMenu.addSeparator();
        helpMenu.add(contactSupportItem);
        helpMenu.add(settingsItem);

        JMenuBar menuBar = new JMenuBar();
        menuBar.add(fileMenu);
        menuBar.add(editMenu);
        menuBar.add(calculationsMenu);
        menuBar.add(helpMenu);
        setJMenuBar(menuBar);
    }

    /** Sets the toolbar */
    private void setToolBar() {
        JToolBar toolBar = new JToolBar();
        JButton addTool = new JButton(
                new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("Icons/add.png"))));
        addTool.setToolTipText("Add to table");
        addTool.addActionListener(event -> {
            addAction();
        });
        JButton resetTool = new JButton(
                new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("Icons/reset.png"))));
        resetTool.setToolTipText("Reset table");
        resetTool.addActionListener(event -> {
            tableModel.resetData();
            table.repaint();
        });
        JButton generateTool = new JButton(
                new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("Icons/generate.png"))));
        generateTool.setToolTipText("Generate random values");
        generateTool.addActionListener(event -> {
            tableModel.generateRandomData();
            table.repaint();
        });
        JButton saveTool = new JButton(
                new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("Icons/save.png"))));
        saveTool.setToolTipText("Save to file");
        saveTool.addActionListener(event -> {
            try {
                tableModel.saveToFile();
            } catch (FileNotFoundException exc) {
                JOptionPane.showMessageDialog(this, exc.getMessage());
            }
        });
        JButton settingsTool = new JButton(
                new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("Icons/settings.png"))));
        settingsTool.setToolTipText("Open settings");
        settingsTool.addActionListener(event -> {
            SettingsDialog dialog = new SettingsDialog(this);
            dialog.display();
        });
        JButton infoTool = new JButton(
                new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("Icons/info.png"))));
        infoTool.setToolTipText("Info");
        infoTool.addActionListener(event -> {
            JOptionPane.showMessageDialog(this, "Author: Slawomir Durawa\nIndex nr.: U-11909");
        });
        JButton helpTool = new JButton(
                new ImageIcon(Objects.requireNonNull(getClass().getClassLoader().getResource("Icons/help.png"))));
        helpTool.setToolTipText("Help");
        helpTool.addActionListener(event->{
            openHelp();
        });
        JButton technicalTool = new JButton(new ImageIcon(
                Objects.requireNonNull(getClass().getClassLoader().getResource("Icons/technical_support.png"))));
        technicalTool.addActionListener(event -> {
            TechnicalSupportContactDialog dialog = new TechnicalSupportContactDialog(this);
            dialog.display();
        });
        technicalTool.setToolTipText("Contact technical support");
        toolBar.add(addTool);
        toolBar.add(resetTool);
        toolBar.add(generateTool);
        toolBar.add(saveTool);
        toolBar.add(settingsTool);
        toolBar.add(infoTool);
        toolBar.add(helpTool);
        toolBar.add(technicalTool);
        toolBar.setFloatable(false);
        JPanel toolBarPanel = new JPanel();
        toolBar.add(toolBarPanel);
        add(toolBar, BorderLayout.NORTH);
    }

    /**
     * Represents an exception of invalid sign put into the input field.
     */
    class InvalidSignException extends Exception {
        InvalidSignException(String message) {
            super(message);
        }
    }

    /**
     * Sets the centre panel of the application.
     */
    private void setCentre() {
        centrePanel = new JPanel();
        centrePanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        centrePanel.setLayout(new BoxLayout(centrePanel, BoxLayout.Y_AXIS));

        setInputField();
        setControls();
        setTable();
        setButtons();
        setResultField();
        setComboOptions();

        add(centrePanel, BorderLayout.CENTER);

    }

    /**
     * Shows the tip popping up on start-up.
     */
    private void showTip() {
        File file = new File("tips.txt");
        ArrayList<String> tips = new ArrayList<String>();
        if (file.exists()) {
            try {
                Scanner fileScanner = new Scanner(file);
                fileScanner.useDelimiter(";");
                while (fileScanner.hasNext()) {
                    tips.add(fileScanner.next());
                }
                fileScanner.close();
            } catch (FileNotFoundException f) {
                f.printStackTrace();
            }
        }
        DefaultTipModel tipModel = new DefaultTipModel();
        if (tips.size() > 0) {
            for (String tip : tips)
                tipModel.add(new DefaultTip("", tip));
        } else {
            tipModel.add(new DefaultTip("", "There is no tip for today cause I cannot find my book with them."));
        }
        JTipOfTheDay tip = new JTipOfTheDay(tipModel);
        tip.showDialog(null);
    }

    /**
     * Sets the left panel of the application.
     */
    private void setWest() {
        JPanel westPanel = new JPanel();

        JOutlookBar bar = new JOutlookBar();
        JPanel centralPanel = new JPanel();
        centralPanel.setLayout(new BoxLayout(centralPanel, BoxLayout.Y_AXIS));
        centralPanel.setPreferredSize(new Dimension(300, 300));
        JCalendarCombo calendar = new JCalendarCombo();
        centralPanel.add(calendar);
        JTextArea textArea = new JTextArea();
        calendar.addDateListener(event -> {
            textArea.setText(new SimpleDateFormat("yyyy-MM-dd").format(calendar.getDate()));
        });
        centralPanel.add(textArea);
        bar.addTab("Calendar", centralPanel);

        chartPanel.setLayout(new BoxLayout(chartPanel, BoxLayout.Y_AXIS));
        bar.add("Chart", tableModel.getChartPanel());

        List<String> list = Arrays.asList("Calendar", "Chart", "Right");
        for (int i = 0; i < bar.getTabCount(); ++i) {
            bar.setToolTipTextAt(i, list.get(i));
        }

        westPanel.add(bar);

        add(westPanel, BorderLayout.WEST);
    }

    /**
     * Sets the input field.
     */
    private void setInputField() {
        JPanel inputContainer = new JPanel();
        inputContainer.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        inputContainer.setLayout(new BoxLayout(inputContainer, BoxLayout.X_AXIS));
        inputLabel = new JLabel("Input");
        inputLabel.setBorder(new EmptyBorder(0, 0, 0, 10));
        inputField = new JTextField();
        inputField.setText("0.0");
        inputField.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

        inputField.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent event) {
                int code = event.getKeyCode();
                if (!(event.getKeyChar() >= '0' && event.getKeyChar() <= '9') && code != 8 && code != 46 && code != 10
                        && !(code > 95 && code < 106)) {
                    reactOnError("Sign " + event.getKeyChar() + " is not allowed. Only numbers are permitted.");
                    String input = inputField.getText();
                    input = input.substring(0, input.length() - 1);
                    inputField.setText(input);
                    logInfo("Forbidden sign input: " + event.getKeyChar());
                } else if (code == 10) {
                    addAction();
                }
            }

            private void reactOnError(String message) {
                JOptionPane.showMessageDialog(null, message);
            }
        });
        inputField.setMaximumSize(new Dimension(Integer.MAX_VALUE, 30));
        inputContainer.add(inputLabel);
        inputContainer.add(inputField);

        centrePanel.add(inputContainer);

    }

    /**
     * Sets row and column controls
     */
    private void setControls() {
        JPanel controlsContainer = new JPanel();
        controlsContainer.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        controlsContainer.setLayout(new BoxLayout(controlsContainer, BoxLayout.X_AXIS));

        JPanel columnPanel = new JPanel();
        columnPanel.setBorder(new EmptyBorder(0, 55, 0, 30));
        columnPanel.setLayout(new BoxLayout(columnPanel, BoxLayout.X_AXIS));
        columnSlider = new JSlider();
        columnSlider.setMinimum(0);
        columnSlider.setMaximum(4);
        columnSlider.setMajorTickSpacing(1);
        columnSlider.setPaintTicks(true);
        columnPanel.add(columnSlider);

        controlsContainer.add(columnPanel);

        centrePanel.add(controlsContainer);

        logInfo("Controls of table set.");
    }

    /**
     * Adds action listeners to sliders.
     */
    private void addListenerToSliders() {
        rowSlider.addChangeListener((ChangeEvent event) -> {
            int row = tableModel.getRowCount() - rowSlider.getValue() - 1;
            table.changeSelection(row, columnSlider.getValue(), false, false);
            logInfo("Table selection changed to " + row + " " + columnSlider.getValue());
        });

        columnSlider.addChangeListener((ChangeEvent event) -> {
            int row = tableModel.getRowCount() - rowSlider.getValue() - 1;
            table.changeSelection(row, columnSlider.getValue(), false, false);
            logInfo("Table selection changed to " + row + " " + columnSlider.getValue());
        });
    }

    /** Sets the table */
    private void setTable() {
        JPanel tablePanel = new JPanel();

        JPanel rowPanel = new JPanel();
        rowPanel.setBorder(new EmptyBorder(15, 0, 15, 0));
        rowPanel.setLayout(new BoxLayout(rowPanel, BoxLayout.Y_AXIS));
        rowSlider = new JSlider(JSlider.VERTICAL);
        rowSlider.setMinimum(0);
        rowSlider.setMaximum(4);
        rowSlider.setMajorTickSpacing(1);
        rowSlider.setPaintTicks(true);
        rowPanel.add(rowSlider);

        tablePanel.add(rowPanel);

        tablePanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        tablePanel.setLayout(new BoxLayout(tablePanel, BoxLayout.X_AXIS));
        table = new JTable(tableModel);

        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
        for (int i = 0; i < tableModel.getColumnCount(); ++i)
            table.getColumnModel().getColumn(i).setCellRenderer(rightRenderer);

        table.getSelectionModel().addListSelectionListener((ListSelectionEvent event) -> {
            rowSlider.setValue(tableModel.getRowCount() - table.getSelectedRow() - 1);
        });
        table.getColumnModel().getSelectionModel().addListSelectionListener((ListSelectionEvent event) -> {
            columnSlider.setValue(table.getSelectedColumn());
        });
        table.setCellSelectionEnabled(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        JScrollPane tableScrollPane = new JScrollPane(table);
        table.getTableHeader().setUI(null);
        for (int i = 0; i < tableModel.getRowCount(); ++i) {
            int rowHeight = (int) (rowSlider.getPreferredSize().getHeight() - 35) / tableModel.getRowCount();
            table.setRowHeight(i, rowHeight);
        }
        tablePanel.add(tableScrollPane);
        centrePanel.add(tablePanel);

        logInfo("Table has been set successfully");
    }

    /** Sets the buttons */
    private void setButtons() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setBorder(new EmptyBorder(0, 26, 0, 0));
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        resetButton = new JButton("Reset");
        resetButton.addActionListener((ActionEvent event) -> {
            CustomTableModel model = (CustomTableModel) table.getModel();
            model.resetData();
            table.repaint();
        });
        buttonPanel.add(resetButton);
        buttonPanel.add(Box.createRigidArea(new Dimension(30, 25)));

        addButton = new JButton("Add value");
        addButton.addActionListener((ActionEvent event) -> {
            addAction();
        });
        buttonPanel.add(addButton);
        buttonPanel.add(Box.createRigidArea(new Dimension(30, 25)));
        resultArea = new JTextArea("Results of calculations will appear here");
        resultArea.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                if (resultArea.getText().equals("Results of calculations will appear here")) {
                    resultArea.setText("");
                    resultArea.setForeground(Color.BLACK);
                }
            }
            @Override
            public void focusLost(FocusEvent e) {
                if (resultArea.getText().isEmpty()) {
                    resultArea.setForeground(Color.GRAY);
                    resultArea.setText("Results of calculations will appear here");
                }
            }
        });
        resultArea.setEditable(false);
        resultArea.setLineWrap(true);
        resultArea.setWrapStyleWord(true);
        resultArea.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        toFileButton = new JButton("Save to file");
        toFileButton.addActionListener((ActionEvent event) -> {
            try {
                tableModel.saveToFile();
            } catch (FileNotFoundException e) {
                JOptionPane.showMessageDialog(this, e.getMessage());
            }
        });
        buttonPanel.add(toFileButton);
        resetButton.setMinimumSize(toFileButton.getPreferredSize());
        resetButton.setPreferredSize(toFileButton.getPreferredSize());
        resetButton.setMaximumSize(toFileButton.getPreferredSize());

        addButton.setMinimumSize(toFileButton.getPreferredSize());
        addButton.setMaximumSize(toFileButton.getPreferredSize());
        addButton.setPreferredSize(toFileButton.getPreferredSize());

        centrePanel.add(buttonPanel);

    }

    /** Sets the result field */
    private void setResultField() {
        JPanel resultFieldPanel = new JPanel();
        resultFieldPanel.setBorder(BorderFactory.createEmptyBorder(10, 38, 10, 10));
        resultFieldPanel.setLayout(new BorderLayout());
        int width = resultArea.getPreferredSize().width;
        int height = resultArea.getPreferredSize().height;
        resultArea.setPreferredSize(new Dimension(width, height + 51));
        JScrollPane resultPane = new JScrollPane(resultArea);
        resultFieldPanel.add(resultPane, BorderLayout.CENTER);
        centrePanel.add(resultFieldPanel);
    }

    /** Represents the model of the ComboBox */
    class OptionsBoxModel extends AbstractListModel implements ComboBoxModel {
        List<String> optionsList = Arrays.asList("Sum", "Average", "Extremes");

        String selection = optionsList.get(0);

        public Object getElementAt(int index) {
            return optionsList.get(index);
        }

        public int getSize() {
            return optionsList.size();
        }

        public void setSelectedItem(Object anItem) {
            selection = (String) anItem;
        }

        public Object getSelectedItem() {
            return selection;
        }
    }

    private void setComboOptions() {
        JPanel comboPanel = new JPanel();
        comboPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        comboPanel.setLayout(new BoxLayout(comboPanel, BoxLayout.X_AXIS));

        JLabel optionsLabel = new JLabel("Calculations");
        optionsLabel.setBorder(new EmptyBorder(0, 0, 0, 10));

        JComboBox optionsBox = new JComboBox();
        optionsBox.setModel(new OptionsBoxModel());
        optionsBox.addActionListener((ActionEvent event) -> {
            String selectedItem = (String) optionsBox.getSelectedItem();
            CustomTableModel tableModel = (CustomTableModel) table.getModel();
            if (selectedItem.equals("Sum")) {
                resultArea.setText(Double.toString(tableModel.sumElements()));
                logInfo("Sum chosen");
            } else if (selectedItem.equals("Average")) {
                resultArea.setText(Double.toString(tableModel.avgElements()));
                logInfo("Average chosen");
            } else if (selectedItem.equals("Extremes")) {
                double min = tableModel.minElement();
                double max = tableModel.maxElement();
                resultArea.setText("Min: " + min + " Max: " + max);
                logInfo("Min and max chosen");
            } else {
                JOptionPane.showMessageDialog(this, "Unknown box item: " + selectedItem);
                logInfo("Unkown option chosen");
            }
        });

        comboPanel.add(optionsLabel);
        comboPanel.add(optionsBox);

        centrePanel.add(comboPanel);
    }

    private void addAction() {
        try {
            Double data = Double.parseDouble(inputField.getText().trim());
            // tableModel.push_back(Double.parseDouble(inputField.getText().trim()));
            tableModel.setData(tableModel.getRowCount() - rowSlider.getValue() - 1, columnSlider.getValue(),
                    Double.parseDouble(inputField.getText().trim()));
            inputField.setText("");
            table.repaint();
        } catch (NumberFormatException exc) {
            JOptionPane.showMessageDialog(this, "Wrong input format in input field");
            logInfo("Wrong input format in input field");
        }
    }

    private void openHelp() {
        try {
            File helpFile = new File("doc/index.html");
            Desktop.getDesktop().browse(helpFile.toURI());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void logInfo(String logMessage){
        statusLabel.setText(logMessage);
        logger.info(logMessage);
    }

}
