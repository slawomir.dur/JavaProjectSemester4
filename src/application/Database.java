package application;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class Database {
    private final String url = "jdbc:sqlite:./settings.db";
    private Connection connection;
    private Statement stmt;

    Database(){
        File file = new File("settings.db");
        if (!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        
        try {
            connection = DriverManager.getConnection(url);
            stmt = connection.createStatement();

            String createQuery = "CREATE TABLE IF NOT EXISTS settings(login varchar(30) not null, password varchar(30) not null)";
            stmt.execute(createQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        connection.close();
        stmt.close();
        super.finalize();
    }

    public boolean setCredentials(Credentials credentials){
        try {
            String updateSettings = "delete from settings";
            stmt.execute(updateSettings);
            updateSettings = "insert into settings(login, password) values(\""+credentials.getLogin()+"\",\""+credentials.getPassword()+"\")";
            stmt.execute(updateSettings);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        catch (Credentials.InvalidCredentialsException e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public Credentials getCredentials(){
        try {
            String get = "select * from settings";
            ResultSet rs = stmt.executeQuery(get);
            if (rs.next()){
                Credentials cr = new Credentials(rs.getString("login"), rs.getString("password"));
                return cr;
            }  
        } catch (SQLException e) {
            e.printStackTrace();
            return new Credentials();
        }
        return new Credentials();
    }
}

class Credentials{

    public class InvalidCredentialsException extends Exception{
        InvalidCredentialsException(){
            super("Credentials invalid");
        }
    }

    private String login;
    private String pwd;
    private boolean isValid = true;

    Credentials(){
        isValid = false;
    }

    Credentials(String login, String password){
        this.login = login;
        this.pwd = password;
    }

    public String getLogin() throws InvalidCredentialsException {
        if (!isValid) 
            throw new InvalidCredentialsException();
        return login;
    }

    public String getPassword() throws InvalidCredentialsException {
        if (!isValid) 
            throw new InvalidCredentialsException();
        return pwd;
    }
}