package application;

import javax.swing.*;
import java.awt.*;
import java.io.FileNotFoundException;

import javax.swing.table.AbstractTableModel;

import java.util.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;
import java.io.PrintWriter;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * This is a table model for the table used in the programme. It enables the
 * user to push new values to it, delete old values exactly in their order of
 * being pushed as well as it facilitates clearing the whole table. This model
 * has functions that calculate the arithmetic average, the sum of all elements
 * and find the minimum and maximum value.
 */
public class CustomTableModel extends AbstractTableModel {
    /**
     * This List<Double> stores the data.
     */
    private List<Double> data = new ArrayList<Double>(Collections.nCopies(25, 0.0));
    private int addIndex = 0;
    final int size = 5;
    private ChartPanel chart = chart();
    private JPanel chartPanel;
    private static final Logger logger = Logger.getLogger(CustomTableModel.class.getName());

    CustomTableModel() {
        DOMConfigurator.configure(Objects.requireNonNull(getClass().getClassLoader().getResource("log4j.xml")));
    }

    /**
     * @return the chart to be displayed in the GUI
     */
    public ChartPanel toChart() {
        return chart;
    }

    /**
     * Sets a chart on a panel.
     * 
     * @param panel represents a JPanel in GUI with a chart
     */
    public void setChartPanel(JPanel panel) {
        chartPanel = panel;
        chartPanel.add(chart);
    }

    /**
     * @return the chart panel
     */
    public JPanel getChartPanel() {
        return chartPanel;
    }

    /**
     * Refreshes the chart on the panel
     */
    public void reloadChart() {
        chartPanel.remove(chart);
        chartPanel.revalidate();
        chart = chart();
        chartPanel.add(chart);
    }

    /**
     * @param d an element to be found in the list
     * @return position of the element or -1 if element not found
     */
    private int position(Double d) {
        List<Double> list = data;
        for (int i = 0; i < list.size(); ++i) {
            if (list.get(i) == d)
                return i;
        }
        return -1;
    }

    /**
     * @param d element whose duplicates are to be removed
     * @return List<Double> devoid of duplicates of d
     */
    private List<Double> removeDuplicates(Double d) {
        List<Double> list = new ArrayList<Double>(data);
        Iterator it = list.iterator();
        int pos = position(d) + 1;
        for (int i = 0; i < pos; ++i) {
            if (it.hasNext())
                it.next();
        }
        while (it.hasNext()) {
            Double elem = (Double) it.next();
            if (elem == d)
                it.remove();
        }
        return list;
    }

    /**
     * @param d the element whose duplicates are to be counted
     * @return the Integer representing the number of d in the dataset
     */
    private Integer count(Double d) {
        int sum = 0;
        for (Double elem : data) {
            if (elem.equals(d))
                ++sum;
        }
        return new Integer(sum);
    }

    /**
     * @return A map of <Double, Integer> values representing the element and its
     *         count in the dataset
     */
    public Map<Double, Integer> getAllElementsCount() {
        List<Double> list = new ArrayList<Double>(data);
        HashMap<Double, Integer> elemCount = new HashMap<Double, Integer>();
        for (int i = 0; i < list.size(); ++i) {
            Integer num = count(list.get(i));
            elemCount.put(list.get(i), num);
            list = removeDuplicates(list.get(i));
        }
        return elemCount;
    }

    /**
     * @return the dataset of the model
     */
    public PieDataset shareDataset() {
        DefaultPieDataset dataSet = new DefaultPieDataset();
        Map<Double, Integer> map = getAllElementsCount();
        for (Map.Entry<Double, Integer> entry : map.entrySet()) {
            dataSet.setValue(entry.getKey(), entry.getValue());
        }
        return dataSet;
    }

    /**
     * @return the ChartPanel with a chart representing values in the dataset
     */
    private ChartPanel chart() {
        PieDataset dataset = shareDataset();
        JFreeChart chart = ChartFactory.createPieChart("Chart", dataset, true, true, false);
        ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new Dimension(300, 300));
        return chartPanel;
    }

    /**
     * Resets data in the dataset with zeroes.
     */
    public void resetData() {
        for (int i = 0; i < data.size(); ++i) {
            data.set(i, 0.0);
        }
        addIndex = 0;
        reloadChart();

        logger.info("Data has been reset");
    }

    /**
     * Pushed an element to the dataset
     * 
     * @param d element to be pushed
     */
    public void push_back(Double d) {
        if (addIndex == 25) {
            addIndex = 0;
        }
        data.set(addIndex++, d);
        reloadChart();

        logger.info(d + " has been added to dataset at index: " + addIndex);
    }

    /**
     * Inserts data under row - column
     * 
     * @param row
     * @param col
     * @param data
     */
    public void insert(int row, int col, Double d) {
        data.set(row * getColumnCount() + col, d);
    }

    /**
     * Counts the arithmetic average of elements.
     * 
     * @return the arithmetic average
     */
    public double avgElements() {
        double avg = sumElements() / data.size();
        logger.info("Average: " + avg);
        return avg;
    }

    /**
     * Sums the elements.
     * 
     * @return the sum of elements
     */
    public double sumElements() {
        return data.stream().mapToDouble(a -> a).sum();
    }

    /**
     * Finds the max element.
     * 
     * @return the max element
     */
    public double maxElement() {
        return Collections.max(data);
    }

    /**
     * Finds the min element.
     * 
     * @return min element
     */
    public double minElement() {
        return Collections.min(data);
    }

    /**
     * Shares the row count of the dataset.
     * 
     * @return the row count of the dataset.
     */
    public int getRowCount() {
        return size;
    }

    /**
     * Gets the column count of the dataset.
     * 
     * @return the column count of the dataset.
     */
    public int getColumnCount() {
        return size;
    }

    /**
     * Gets the value at particular position.
     * 
     * @return the value at particular position of the dataset.
     */
    public Object getValueAt(int row, int column) {
        return data.get(row * size + column);
    }

    /** Saves data to file */
    public boolean saveToFile() throws FileNotFoundException {
        JFileChooser chooser = new JFileChooser();
        int answer = chooser.showSaveDialog(null);
        if (answer == JFileChooser.APPROVE_OPTION) {
            String path = chooser.getSelectedFile().getAbsolutePath();
            if (!path.endsWith(".txt"))
                path += ".txt";
            try {
                File file = new File(path);
                if (!file.exists()) {
                    file.createNewFile();
                }
                PrintWriter writer = new PrintWriter(new FileOutputStream(file, true));
                for (int i = 0; i < getRowCount(); ++i) {
                    for (int j = 0; j < getColumnCount(); ++j)
                        writer.print(getValueAt(i, j) + "  ");
                    writer.println();
                }
                writer.flush();
                writer.close();
                logger.info("Table content written to file");
            } catch (IOException exc) {
                exc.printStackTrace();
                return false;
            }
            return true;
        }
        return false;

    }

    /**
     * Sets particular data on row and col
     * 
     * @param row
     * @param col
     * @param d   value to be set
     */
    public void setData(int row, int col, Double d) {
        data.set(row * getColumnCount() + col, d);
        logger.info("Set " + d + " to " + '[' + row + ',' + col + ']');
    }

    /**
     * Generates data randomly
     */
    public void generateRandomData() {
        Random random = new Random();
        for (int i = 0; i < data.size(); ++i) {
            data.set(i, (double) random.nextInt(10));
        }
        reloadChart();
        logger.info("Generated random values");
    }

    public void loadFromFile(Component parent) throws IOException {
        JFileChooser fc = new JFileChooser();
        int chosen_option = fc.showOpenDialog(parent);
        if (chosen_option == JFileChooser.APPROVE_OPTION){
            File file = fc.getSelectedFile();
            if (!file.exists())
                throw new IOException("Cannot open file");
            Scanner scanner = new Scanner(file);
            for (int i = 0; i < 25; ++i){
                if (i%5 == 0 && i != 0){
                    scanner.nextLine();
                }
                data.set(i, scanner.nextDouble());
                
            }
            scanner.close();
        }
        
    }
}
